(defconstant empty 0 "An empty space")
(defconstant red 1 "Player 1")
(defconstant yellow 2 "Player 2")

;;;successor function comes up with all the next moves
;;;adds all next moves to the state
;;;having three in a row is priced higher than two in a row, pick move with highest price
;;;consider moves on opposite player's part that minimizes our score
;;;function that takes move and produces a board to see what it looks like
;;;price function that determines worth of board
;;;search function alread have
;;;as in trip function we're building up paths
;;;need to keep track of paths, not just say who wins at the end but return what happened in the game
;(defconstant all-directions '(-7 -6 -5 -1 1 5 6 7))

(deftype board () '(simple-array integer (42)))


(defun initial-board ()
  (make-array 42
	      :element-type 'integer
	      :initial-element empty))

;(defun opponent (player) (if (eql player red) yellow red))

(defun choose-col (board)
  (let ((choice (* (random 6) 6)))
  (if (= (aref board choice) 0) choice (choose-col board))))

;;;(defun make-move (board choice)
;;;  (cond (if (not (aref board choice)) drop-piece(board choice))
;;;	(t (format t "Column Full"))))

(defun drop-piece (board choice)
  "Drops the piece from the chosen column to the first available slot, if the slot is open"
  (loop for i from choice to (+ choice 5)
     when (or (= (or 1 2) (aref board i)) (= i (+ choice 6)))
     return (- i 1)))
  

;(defparameter board initial-board())
					;(defparameter choice choose-col())
(defun cost-function (board)
    (choose-col board))

(defun connect-four ()
  "Play an entire connect-four game based on the chosen cost-function"
  (let ((board (initial-board)))
    (pprint board)
  ;(defparameter choice (cost-function))
  (loop for i from 0
     do
       ;(progn;(setf choice (cost-function))
	 (setf (aref board (drop-piece board 0)) red)
       ;(setf choice (cost-function))
	 ;(setf (aref board (drop-piece board 6)) yellow))
     until (= i 42))))

;(defun print-board(board)
 ;   )
  ;(setf player red)
  ;
;  (loop for i
       
 ;      (setf (aref board choice) player)
